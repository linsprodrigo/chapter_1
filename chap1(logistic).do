** Rodrigo Lins' Political Science PhD dissertation - Chapter 1 **

	** ANALYSIS SECTION	**

**	Logistic Regressions with panel data

clear
use "(...)\logit_data.dta"

**	Adjusting the data for panel analysis
xtset cnum year, yearly

**	ACLP logistic regression with panel data analysis

*Fixed Effects
xtlogit aclp gdp_pc urb_pop tensys, fe
estimates store fe

*Random Effects
xtlogit aclp gdp_pc urb_pop tensys, re
estimates store re

*Hausman Test
hausman fe

	** LOGISTIC SECTION	**

**	Freedom House logistic regression with panel data analysis

*Fixed Effects
xtlogit fh_dummy gdp_pc urb_pop tensys, fe
estimates store fe

*Random Effects
xtlogit fh_dummy gdp_pc urb_pop tensys, re
estimates store re

*Hausman Test
hausman fe

**	Polity IV logistic regression with panel data analysis

*Fixed Effects
xtlogit polity_dummy gdp_pc urb_pop tensys, fe
estimates store fe

*Random Effects
xtlogit polity_dummy gdp_pc urb_pop tensys, re
estimates store re

*Hausman Test
hausman fe

**	V-Dem logistic regression with panel data analysis

*Fixed Effects
xtlogit vdem_dummy gdp_pc urb_pop tensys, fe
estimates store fe

*Random Effects
xtlogit vdem_dummy gdp_pc urb_pop tensys, re
estimates store re

*Hausman Test
hausman fe
